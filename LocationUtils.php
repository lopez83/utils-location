<?php

/**
 * Utils class for working out:
 *  - distance between two coordinates.
 *  - getting random location inside an area.
 *  - get a square area from a point.
 *
 * @author Óscar López
 *
 */
class LocationUtils {
	const EARTH_RADIUS = 6371; // kms

	/**
	 * Get a random location which distances less than $radius from the given point.
	 *
	 * @param unknown $lat
	 *        	latitude for the point
	 * @param unknown $lon
	 *        	longitude for the point
	 * @param number $radius
	 *        	max distance in meters from the point
	 * @return multitype:number unknown
	 */
	public static function getRandLocation($lat, $lon, $radius) {
		$lng_min = $lon - $radius / abs ( cos ( deg2rad ( $lat ) ) * 110 );
		$lng_max = $lon + $radius / abs ( cos ( deg2rad ( $lat ) ) * 110 );
		$lat_min = $lat - ($radius / 110);
		$lat_max = $lat + ($radius / 110);

		$boundaries = self::getBoundaries ( $lat, $lon, $radius );
		$lat_min = $boundaries ['lat'] ['bottom'];
		$lat_max = $boundaries ['lat'] ['top'];
		$lng_min = $boundaries ['lon'] ['left'];
		$lng_max = $boundaries ['lon'] ['right'];

		$decimals = pow ( 10, 6 );

		$lat_s = rand ( 0, ($lat_max - $lat_min) * $decimals ) / $decimals + $lat_min;
		$lon_s = rand ( 0, ($lng_max - $lng_min) * $decimals ) / $decimals + $lng_min;

		return array (
				'lat' => $lat_s,
				'lon' => $lon_s
		);
	}

	/**
	 * Returns boundaries for a square having lat/lon as the center of it
	 * and a distance from the corners to the center.
	 *
	 * @param unknown $lat
	 *        	latitude for the central point
	 * @param unknown $lon
	 *        	longitude for the central point
	 * @param unknown $distance
	 *        	distance in meters to the point
	 * @return multitype:multitype:number
	 */
	private function getBoundaries($lat, $lon, $distance) {
		$kmFactor = 157;
		return array (
				'lat' => array (
						'bottom' => $lat - ($distance / $kmFactor),
						'top' => $lat + ($distance / $kmFactor)
				),
				'lon' => array (
						'left' => $lon - ($distance / abs ( cos ( deg2rad ( $lat ) ) * $kmFactor )),
						'right' => $lon + ($distance / abs ( cos ( deg2rad ( $lat ) ) * $kmFactor ))
				)
		);
	}

	/**
	 * Get the distance (in ) from 2 points.
	 *
	 * @param unknown $latO
	 *        	origin latitude
	 * @param unknown $lonO
	 *        	origin longitude
	 * @param unknown $latD
	 *        	destiny latitude
	 * @param unknown $lonD
	 *        	destiny longitude
	 * @return number
	 */
	public static function getDistance($latO, $lonO, $latD, $lonD) {
		$dLat = deg2rad ( abs ( $latO - $latD ) );
		$dLon = deg2rad ( abs ( $lonO - $lonD ) );

		$radLatO = deg2rad ( $latO );
		$radLatD = deg2rad ( $latD );

		$a = sin ( $dLat / 2 ) * sin ( $dLat / 2 ) + sin ( $dLon / 2 ) * sin ( $dLon / 2 ) * cos ( $radLatD ) * cos ( $radLatO );
		$c = 2 * atan2 ( sqrt ( $a ), sqrt ( 1 - $a ) );
		return self::EARTH_RADIUS * $c;
	}
}